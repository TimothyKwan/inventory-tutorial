using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScrollViewPos : MonoBehaviour
{
    private ScrollRect myScrollRect;
    // Start is called before the first frame update
    void Start()
    {
        myScrollRect = GetComponent<ScrollRect>();
        myScrollRect.verticalNormalizedPosition = 1f;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
