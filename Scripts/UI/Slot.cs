using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;


//namespace UI
//{
public class Slot : MonoBehaviour
{
    public Action<Slot> onMouseEnterCallback;
    public Action onMouseExitCallback;

    [SerializeField] private Image icon;
    [SerializeField] private TMP_Text amountDisplay;
    [SerializeField] private Button button;
    private Item slotItem;


    //Update slot info
    public void updateSlot(Item item)
    {
        slotItem = item;
        icon.sprite = item.getItemData().icon;
        amountDisplay.text = item.getAmount().ToString();
    }

    //Clear slot reference
    public void clearSlot()
    {
        icon = null;
        amountDisplay.text = "";
        slotItem = null;
    }

    public Item getSlotItem()
    {
        return slotItem;
    }

    public Button GetButton()
    {
        return button;
    }
}
//}
