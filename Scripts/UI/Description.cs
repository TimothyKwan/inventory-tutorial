using UnityEngine;
using UnityEngine.UI;
using TMPro;


namespace UI
{
    public class Description : MonoBehaviour
    {
        [SerializeField] private Button UseButton;
        [SerializeField] private Image icon;
        [SerializeField] private TMP_Text itemName;
        [SerializeField] private TMP_Text description;

        
        private Item currentItem;


        #region General

        //update the description panel
        public void updateDescription(Slot itemSlot)
        {
            Debug.Log("Update Description");
            if (itemSlot.getSlotItem() != null)
            {
                currentItem = itemSlot.getSlotItem();
                icon.sprite = currentItem.getItemData().icon;
                itemName.text = currentItem.getItemData().itemName;
                description.text = currentItem.getItemData().description;
                if(UseButton.onClick !=null)
                {
                    UseButton.onClick.RemoveAllListeners();
                }
                
                UseButton.onClick.AddListener(currentItem.useItem);
            }
        }

        public void clear()
        {
            icon.sprite = null;
            itemName.text = "";
            description.text = "";
        }
        #endregion
    }
}
