using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UI;
using Utilities;
using Data.Enums;
using Inventory;


[RequireComponent(typeof(ContentSizeFitter))]
[RequireComponent(typeof(RectTransform))]
public class SlotUIController : MonoBehaviour
{
    [SerializeField] private int fitPageItemAmount = 48;
    [SerializeField] private GameObject SlotPrefab;
    [SerializeField] private int PageVerticalSize = 792;
    [SerializeField] private Description descriptionUI;

    private ContentSizeFitter sizeFitter;
    private RectTransform UITransform;
    private List<Slot> SlotObjectList;


    //for testing purpose
    [SerializeField] private ScriptableItem testItem;
    [SerializeField] private ScriptableItem testItem2;
    private List<Item> testList;//for testing purpose

    // Start is called before the first frame update
    void Start()
    {
        sizeFitter = GetComponent<ContentSizeFitter>();
        sizeFitter.enabled = false;
        UITransform = GetComponent(typeof(RectTransform)) as RectTransform;
        UITransform.sizeDelta = new Vector2(UITransform.sizeDelta.x, 792);
        PlayerInventory.Instance.onItemChangeCallback += updateSlots;
        //Initialize 50 slots for item
        SlotObjectList = new List<Slot>();
        for(int i = 0;i< 50; i++)
        {
            
            //newSlot.GetComponent<Button>().OnPointerEnter()
            /* TODO: add description update to slot on enter and add clear to slot on exit*/
            generateSlot();
        }

        //for testing purpose
        testList = new List<Item>();
        for(int i = 0; i < 20; i++)
        {
            int itemNumber = Random.Range(0, 2);
            testList.Add(Factory.CreateNewItem(ItemType.Potion, (itemNumber == 0)? testItem: testItem2, 1));
        }
    }

    // Update is called once per frame
    void Update()
    {
        //for testing purpose
        if (Input.GetKeyDown(KeyCode.I))
        {
            updateSlots(testList);
        }
    }

    #region UI Adjustment
    private void adjustSize(int itemAmount)
    {
        if(itemAmount >= 48)
        {
            sizeFitter.enabled = true;
        }
        else
        {
            sizeFitter.enabled = false;
        }
    }
    #endregion

    #region General
    public void updateSlots(List<Item> itemList)
    {
        adjustSize(itemList.Count);
        if (itemList.Count >= SlotObjectList.Count)
        {
            var i = (itemList.Count - SlotObjectList.Count) * 2 + 10;
            for (int j = 0; j < i; j++)
            {
                generateSlot();
            }
        }

        for (int i = 0; i < itemList.Count; i++)
        {
            //Debug.Log(SlotObjectList[i] == null);
            SlotObjectList[i].gameObject.SetActive(true);
            SlotObjectList[i].updateSlot(itemList[i]);
        }
    }

    /*helper method that generates one slot and put it into the slot list*/
    private void generateSlot()
    {
        GameObject newSlot = Instantiate(SlotPrefab, gameObject.transform);
        Slot tmp = newSlot.GetComponent<Slot>();
        tmp.GetButton().onClick.AddListener(() => { descriptionUI.updateDescription(tmp); });
        SlotObjectList.Add(newSlot.GetComponent<Slot>());
        newSlot.gameObject.SetActive(false);
    }
    #endregion
}
