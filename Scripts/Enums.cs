
namespace Data.Enums
{
    public enum ItemType
    {
        Potion,
        Material,
        Other
    }
    
    /*state for if the loot container will add all items immediately after it's destroyed*/
    public enum DestroyType
    {
        None,
        Immediate
    }
}
