using System.Collections;
using System.Collections.Generic;
using Data.Enums;
using UnityEngine;
using UnityEngine.UI;

/*stores the data of the item*/
[CreateAssetMenu(fileName ="new item", menuName ="Item/create new item")]
public class ScriptableItem : ScriptableObject
{
    public int HP;
    public int MP;
    public string itemName;
    public bool canStack;
    public string description;
    public ItemType itemType;
    //UI
    public Sprite icon;
}

