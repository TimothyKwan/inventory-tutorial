using System.Collections.Generic;
using Utilities;
using System;
using System.Linq;

namespace Inventory
{
    //TODO: fix the item ID problem where item's ID is different every time it is created and cause duplicated items
    public sealed class PlayerInventory : IECharacterInventory
    {
        #region singleton
        private static readonly PlayerInventory instance = new PlayerInventory();

        static PlayerInventory() { }
        private PlayerInventory() { }

        public static PlayerInventory Instance
        {
            get
            {
                return instance;
            }
        }
        #endregion

        public Action<List<Item>> onItemChangeCallback;

        //Dictionary that contains a list of item name and the Dictionaries that contain the itemID and the item object
        private Dictionary<string, Dictionary<string, Item>> itemList = new Dictionary<string, Dictionary<string, Item>>();


        /*TODO: update Inventory UI and UI open and close*/
        /*This add method expect to only add new item(s) into the dictionary*/
        public void AddItem(ScriptableItem newItem, int ItemAmount = 1)
        {
            Item tmp = Factory.CreateNewItem(newItem.itemType, newItem, ItemAmount);
            if (!newItem.canStack)
            {
                itemList.Add(tmp.getItemData().itemName, new Dictionary<string, Item>() { { tmp.getID(), tmp } });
            }
            else
            {
                itemList[newItem.itemName][tmp.getID()].amountChange(ItemAmount);
            }
        }

        /*This is an overload method for AddItem. This method check the list before adding items from Loot Container*/
        public void AddItem(Item newItem, int ItemAmount = 1)
        {
            if(newItem.getItemData().canStack && itemList.ContainsKey(newItem.getItemData().itemName))
            {//if the item can be stacked and exist in the dictionary then change the amount
                var tmp = itemList[newItem.getItemData().itemName].Values.ElementAt(0);
                tmp.amountChange(ItemAmount);
            }
            else if(itemList.ContainsKey(newItem.getItemData().itemName))
            {//add directly if it does not exist or cannot stack
                itemList[newItem.getItemData().itemName].Add(newItem.getID(), newItem);
            }
            else
            {
                itemList.Add(newItem.getItemData().itemName, new Dictionary<string, Item>() { { newItem.getID(), newItem } });
            }

            string[] nameList = itemList.Keys.ToArray();
            List<Item> items = new List<Item>();

            foreach(string itemName in nameList)
            {
                Dictionary<string, Item> tmpDict = itemList[itemName];
                items.AddRange(tmpDict.Values);
            }
            
            onItemChangeCallback.Invoke(items);
        }

        public void RemoveItem(Item targetItem, int amount = 0)
        {
            if (targetItem.getItemData().canStack)
            {
                if(amount <= 0)
                {//if no input then remove the item by name
                    itemList.Remove(targetItem.getItemData().itemName);
                }
                else
                {
                    itemList[targetItem.getItemData().itemName][targetItem.getID()].amountChange(-amount);
                }
            }
            else
            {
                itemList[targetItem.getItemData().itemName].Remove(targetItem.getID());
            }

            string[] nameList = itemList.Keys.ToArray();
            List<Item> items = new List<Item>();

            foreach (string itemName in nameList)
            {
                Dictionary<string, Item> tmpDict = itemList[itemName];
                items.AddRange(tmpDict.Values);
            }
            onItemChangeCallback.Invoke(new List<Item>(items));
        }

    }
}
