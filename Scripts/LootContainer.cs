using System;
using System.Collections.Generic;
using UnityEngine;
using Utilities;
using Data.Enums;

namespace Inventory
{
    public class LootContainer : MonoBehaviour, IECharacterInventory
    {
        //this is for Editor
        [SerializeField] private string ContainerName;
        [SerializeField] private DestroyType destroyType;
        [SerializeField] private int MaxNumber;
        [SerializeField] private int MinNumber;
        [SerializeField] private List<ScriptableItem> itemList;
        private List<Item> itemObjects = new List<Item>();

        private void Start()
        {
            if (MinNumber <= 0)
            {
                MinNumber = 1;
            }
            //generate the item objects and it's amount based on the list of data
            foreach (ScriptableItem i in itemList)
            {
                if (i != null)
                {
                    itemObjects.Add(Factory.CreateNewItem(i.itemType, i, UnityEngine.Random.Range(MinNumber, MaxNumber + 1)));
                }
            }
        }

        public void AddItem(ScriptableItem newItem, int ItemAmount = 1)
        {
            itemObjects.Add(Factory.CreateNewItem(newItem.itemType, newItem, ItemAmount));
        }


        public void RemoveItem(Item targetItem, int amount = 0)
        {
            if (amount <= 0)
            {//remove item if there's no input or less then 1
                itemObjects.Remove(targetItem);
            }
            else
            {//if input is < 0 then change the amount
                if (!targetItem.getItemData().canStack)
                {//if the item cannot stack then remove
                    itemObjects.Remove(targetItem);
                }
                else
                {//if the stack can stack then remove
                    itemObjects.Find(x => x.getItemData().itemName.Equals(targetItem.getItemData().itemName)).amountChange(-amount);
                }
            }
        }

        private void OnDestroy()
        {
            if (destroyType == DestroyType.Immediate)
            {
                //add everything into the player's inventory
                foreach(Item i in itemObjects)
                {
                    //Debug.Log("Item added: " + i.getItemData().itemName);
                    PlayerInventory.Instance.AddItem(i, UnityEngine.Random.Range(MinNumber, MaxNumber + 1));
                }
            }
        }
    }
}
