

namespace Inventory
{
    public interface IECharacterInventory
    {
        /// <summary>
        /// This method is to allow items to be added into the list or change the amount
        /// </summary>
        /// <param name="newItem">Item data</param>
        /// <param name="ItemAmount">Number of items to be added</param>
        public void AddItem(ScriptableItem newItem, int ItemAmount = 1);

        /// <summary>
        /// This method is to allow items to be removed from the list or change the amount
        /// </summary>
        /// <param name="targetItem"></param>
        /// <param name="amount"></param>
        public void RemoveItem(Item targetItem, int amount = 0);
    }
}
