using System.Collections;
using System.Collections.Generic;
using UnityEngine;


//This is a script for testing purpose only
public class Destroy : MonoBehaviour
{
    [SerializeField] private string targetTag;
    // Update is called once per frame
    void Update()
    {
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == targetTag)
        {
            Destroy(collision.gameObject);
        }
    }
}
