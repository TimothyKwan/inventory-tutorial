public abstract class Item
{
    private ScriptableItem itemData;
    private int amount;
    private string itemID;

    /*this indicates what will happen when the item is used*/
    public abstract void useItem();

    public void setScriptableItem(ScriptableItem item)
    {
        itemData = item;
    }

    public ScriptableItem getItemData()
    {
        return itemData;
    }

    public void amountChange(int num)
    {
        amount += num;
    }

    public int getAmount()
    {
        return amount;
    }

    public void setitemID(string id)
    {
        itemID = id;
    }

    public string getID()
    {
        return itemID;
    }

    public override bool Equals(object other)
    {
        Item temp = other as Item;
        return temp.getID() == itemID;
    }

    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
