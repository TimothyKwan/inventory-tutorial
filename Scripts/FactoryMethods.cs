using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Data.Enums;
using System;
using System.Diagnostics;

namespace Utilities
{
    public static class Factory
    {
        public static Item CreateNewItem(ItemType itemType, ScriptableItem data, int amount)
        {

            switch (itemType)
            {
                case ItemType.Potion:
                    var newPotion = new Potion();
                    newPotion.setScriptableItem(data);
                    newPotion.setitemID(GenerateItemID());
                    newPotion.amountChange(amount);
                    return newPotion;
                default:
                    break;
            }
            return null;
        }

        public static string GenerateItemID()
        {
            Guid newID = Guid.NewGuid();
            return newID.ToString();
        }
    }

}
